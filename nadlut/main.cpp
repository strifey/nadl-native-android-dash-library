#include "mpdut.h"
#include "stagefrightut.h"
#include "nw_ut.h"

int main(int argc, char* argv[]){
	printf("\n========Starting Tests!========\n");
	// if(argc > 1){
	// 	printf("\tMPD Tests: ========\n\n");
	// 	MPD mpd = parse_mpd(argv[1]);
	// 	android::Vector<Period>& periods = mpd.getPeriods();
	// 	for(unsigned int periodIndex = 0; periodIndex < periods.size(); periodIndex++){
	// 		Period period = periods.itemAt(periodIndex);
	// 		printPeriod(period, periodIndex);
	// 		android::Vector<AdaptSet>& asets = period.getAsets();
	// 		for(unsigned asetIndex = 0; asetIndex < asets.size(); asetIndex++){
	// 			AdaptSet aset = asets.itemAt(asetIndex);
	// 			printAset(aset, asetIndex);
	// 			android::Vector<Repr>& reprs = aset.getReps();
	// 			for(unsigned int reprIndex = 0; reprIndex < reprs.size(); reprIndex++){
	// 				Repr repr = reprs.itemAt(reprIndex);
	// 				printRepr(repr, reprIndex);
	// 				android::Vector<Segment>& segments = repr.getSegs();
	// 				for(unsigned int segmentIndex = 0; segmentIndex < segments.size(); segmentIndex++){
	// 					Segment segment = segments.itemAt(segmentIndex);
	// 					printSegment(segment, segmentIndex);
	// 				}
	// 			}
	// 		}
	// 	}
	// }
	// else{
	// 	printf("No mpd file inputted\n");
	// }
	//printf("\n\t\tStagefright Tests: ========\n\n\n");
	//test_DataSource();

	printf("\tMPD Tests: ========\n\n");
	if(argc > 1){
		testCar(argv[1]);
	}
	else{
		testCar(NULL);
	}
	// printf("\tNetworking Tests: ========\n\n");
	// test_nwRoutines();


	// printf("\tMPD Car Tests: ========\n\n");
	// testCar();
	return 0;
}
