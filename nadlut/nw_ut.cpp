#include "nw_ut.h"

namespace Nadl{
	void test_nwRoutines(){
		printf("inside of func call\n");
		char* getRequest = createGetRequest("i.imgur.com", "/qIwia.gif");
		nw_info test_info;
		test_info.url = "i.imgur.com";
		test_info.port = "80";
		printf("String size of GET request: %lu\n\n", strlen(getRequest));
		int sock = connectToServer(&test_info);
		if(sock < 1)
			exit(-1);
		size_t ssize = sendString(sock, getRequest);
		char* filename = recvFile(sock, "logo.png");
		return;
	}
}
