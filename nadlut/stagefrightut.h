#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <media/stagefright/DataSource.h>
#include <media/stagefright/MediaSource.h>
#include <media/stagefright/MediaExtractor.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MetaData.h>
// #include <media/stagefright/OMXCodec.h>
// #include <media/stagefright/OMXClient.h>
#include <media/stagefright/AudioPlayer.h>
#include "StagefrightPlayer.h"
#include <utils/String16.h>
#include <utils/Vector.h>
#include <utils/Errors.h>
#include <utils/Log.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#ifndef STAGEFRIGHT_UT_
#define STAGEFRIGHT_UT_

void test_DataSource();
void test_AwesomePlayer();

#endif
