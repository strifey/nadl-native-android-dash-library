LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
LOCAL_SRC_FILES := main.cpp mpdut.cpp stagefrightut.cpp nw_ut.cpp
LOCAL_C_INCLUDES := system/extras/libtinyxml system/extras/libnadl \
frameworks/av/media/libmediaplayerservice
LOCAL_SHARED_LIBRARIES := libstdc++ liblog libutils libbinder libcutils \
	libdl libtinyxml libnadl libstagefright libmediaplayerservice
LOCAL_MODULE := nadlut
include $(BUILD_EXECUTABLE)
