#include "mpdut.h"

void printPeriod(Period& period, unsigned int& index){
	printf("Period %u\n", index);
	printf("Period's start = %s\n", period.getStart());
	printf("Period's duration = %s\n", period.getDuration());
	printf("Size of asets is %u\n", (unsigned int) period.getAsets().size());
}

void printAset(AdaptSet& aset, unsigned int& index){
	printf("Asets %u\n", index);
	printf("Aset's subsegmentalign = %d\n", aset.getSubsegment());
	//printf("Address of mimeType is %u\n", aset.getMimeType());
	if(aset.getMimeType() != NULL)
		printf("Aset's mimeType = %s\n", aset.getMimeType());
	else
		printf("Aset's mimeType = NULL\n");
	printf("Size of repr is %u\n", (unsigned int) aset.getReps().size());
}

void printRepr(Repr& repr, unsigned int& index){
	printf("Repr %u\n", index);
	printf("Repr ID is %u\n", repr.getID());
	printf("Repr width is %u\n", repr.getWidth());
	printf("Repr height is %u\n", repr.getHeight());
	printf("Repr bandwidth is %u\n", repr.getBandwidth());
	printf("Repr BaseURL is %s\n", repr.getBaseURL());
	printf("Repr codecs is %s\n", repr.getCodecs());
	printf("Repr mimeType is %s\n", repr.getMimeType());
	printf("Repr audioSamplingRate is %u\n", repr.getAudioSamplingRate());
	printf("Repr startWithSAP is %d\n", repr.getStartWithSAP());
	printf("Size of segments is %u\n", (unsigned int)repr.getSegs().size());
}

void printSegment(Segment& segment, unsigned int& index){
	std::pair<unsigned int, unsigned int>& indexRange = segment.getIndexRange();
	std::pair<unsigned int, unsigned int>& initRange = segment.getInitRange();
	printf("Segment %u\n", index);
	printf("Segment's indexRange is [%u, %u]!\n", indexRange.first, indexRange.second);
	printf("Segment's initRange is [%u, %u]!\n", initRange.first, initRange.second);
	printf("Segment's indexRangeExact is %d\n", segment.getIndexRangeExact());
}

void printFailure(char* variable, char const* actualValue){
	printf("FAILURE: %s should have been %s\n", variable, actualValue);
}

void printAll(char* filePath){
	printf("\nStarting Tests!\n");
		if(filePath != NULL && strlen(filePath) > 0){
			MPD mpd = parse_mpd(filePath);
			android::Vector<Period>& periods = mpd.getPeriods();
			for(unsigned int periodIndex = 0; periodIndex < periods.size(); periodIndex++){
				Period period = periods.itemAt(periodIndex);
				printPeriod(period, periodIndex);
				android::Vector<AdaptSet>& asets = period.getAsets();
				for(unsigned asetIndex = 0; asetIndex < asets.size(); asetIndex++){
					AdaptSet aset = asets.itemAt(asetIndex);
					printAset(aset, asetIndex);
					android::Vector<Repr>& reprs = aset.getReps();
					for(unsigned int reprIndex = 0; reprIndex < reprs.size(); reprIndex++){
						Repr repr = reprs.itemAt(reprIndex);
						printRepr(repr, reprIndex);
						android::Vector<Segment>& segments = repr.getSegs();
						for(unsigned int segmentIndex = 0; segmentIndex < segments.size(); segmentIndex++){
							Segment segment = segments.itemAt(segmentIndex);
							printSegment(segment, segmentIndex);
						}
					}
				}
			}
		}
		else{
			printf("No mpd file inputted\n");
		}
}
void testCar(char* filePath){
	MPD mpd;
	if(filePath != NULL)
		mpd = parse_mpd(filePath);
	else
		mpd = parse_mpd(carPath);
	android::Vector<Period>& periods = mpd.getPeriods();
	Period period = periods.itemAt(0);

	//Check Period here
	//<Period duration="PT0H3M1.63S" start="PT0S">
	printf("Period's start = %s\n", period.getStart());
	if(strcmp(period.getStart(), "PT0S") != 0){
		printFailure("start", "PT0S");
	}
	printf("Period's duration = %s\n", period.getDuration());
	if(strcmp(period.getDuration(), "PT0H3M1.63S") != 0){
		printFailure("duration", "PT0H3M1.63S");
	}
	printf("Size of asets is %u\n", (unsigned int) period.getAsets().size());
	if(period.getAsets().size() != 2){
		printFailure("Size of asets", "2");
	}

	//Check Asets here:
	int asetIndex = 0;
	android::Vector<AdaptSet> asets = period.getAsets();
	AdaptSet aset = asets.itemAt(0);


	//<AdaptationSet>
	printf("Aset's subsegmentalign = %d\n", aset.getSubsegment());
	if(aset.getSubsegment() != 0){
		printFailure("subsegmentalign", "0");
	}
	printf("Aset's mimeType = %s\n", aset.getMimeType());
	if(aset.getMimeType() != NULL){
		printFailure("mimeType", NULL);
	}

	//printf("about to check the size of repr\n");
	printf("Size of repr is %u\n", (unsigned int) aset.getReps().size());
	if(aset.getReps().size() != 6){
		printFailure("Size of repr", "6");
	}

	//printf("About to check the repr vector\n");
	int reprIndex = 0;
	android::Vector<Repr>& reprs = aset.getReps();
	// <Representation bandwidth="4190760" codecs="avc1.640028" height="1080" id="1" mimeType="video/mp4" width="1920">
	Repr repr = reprs.itemAt(reprIndex);
	printf("Repr ID is %u\n", repr.getID());
	if(repr.getID() != 1){
		printFailure("ID", "1");
	}
	printf("Repr width is %u\n", repr.getWidth());
	if(repr.getWidth() != 1920){
		printFailure("width", "1920");
	}
	printf("Repr height is %u\n", repr.getHeight());
	if(repr.getHeight() != 1080){
		printFailure("height", "1080");
	}
	printf("Repr bandwidth is %u\n", repr.getBandwidth());
	if(repr.getBandwidth() != 4190760){
		printFailure("bandwidth", "4190760");
	}
	printf("Repr BaseURL is %s\n", repr.getBaseURL());
	if(repr.getBaseURL() == NULL || strcmp(repr.getBaseURL(), "car-20120827-89.mp4") != 0){
		printFailure("BaseURL", "car-20120827-89.mp4");
	}

	printf("Repr codecs is %s\n", repr.getCodecs());
	if(strcmp(repr.getCodecs(), "avc1.640028") != 0){
		printFailure("Codecs", "avc1.640028");
	}

	printf("Repr mimeType is %s\n", repr.getMimeType());
	if(strcmp(repr.getMimeType(), "video/mp4") != 0){
		printFailure("mimeType", "video/mp4");
	}
	printf("Repr audioSamplingRate is %u\n", repr.getAudioSamplingRate());
	if(repr.getAudioSamplingRate() != 0){
		printFailure("audioSamplingRate", "0");
	}
	printf("Repr startWithSAP is %d\n", repr.getStartWithSAP());
	if(repr.getStartWithSAP() != 0){
		printFailure("startWithSAP", "0");
	}
	printf("Size of segments is %u\n", (unsigned int)repr.getSegs().size());
	if(repr.getSegs().size() != 1){
		printFailure("Size of segments", "1");
	}



	int segmentIndex = 0;
	android::Vector<Segment>& segments = repr.getSegs();

	//<SegmentBase indexRange="674-1149">
          // <Initialization range="0-673" />
	Segment segment = segments.itemAt(segmentIndex);
	std::pair<unsigned int, unsigned int> indexRange = segment.getIndexRange();
	std::pair<unsigned int, unsigned int> initRange = segment.getInitRange();
	printf("Segment's indexRange is [%u, %u]!\n", indexRange.first, indexRange.second);
	if(indexRange.first != 674 || indexRange.second != 1149){
		printFailure("indexRange", "[674, 1149]");
	}
	printf("Segment's initRange is [%u, %u]!\n", initRange.first, initRange.second);
	if(initRange.first !=0 || initRange.second != 673){
		printFailure("initRange", "[0, 673]");
	}
	printf("Segment's indexRangeExact is %d\n", segment.getIndexRangeExact());
	if(segment.getIndexRangeExact() != 0){
		printFailure("indexRangeExact", "0");
	}



	AdaptSet aset2 = asets.itemAt(1);
	printf("Aset's subsegmentalign = %d\n", aset2.getSubsegment());
	if(aset2.getSubsegment() != 0){
		printFailure("subsegmentalign", "0");
	}
	printf("Aset's mimeType = %s\n", aset2.getMimeType());
	if(aset2.getMimeType() != NULL){
		printFailure("mimeType", NULL);
	}

	printf("Size of repr is %u\n", (unsigned int) aset2.getReps().size());
	if(aset2.getReps().size() != 3){
		printFailure("Size of repr", "3");
	}

	android::Vector<Repr> reprs2 = aset2.getReps();
	Repr repr2 = reprs2.itemAt(0);

	printf("Repr ID is %u\n", repr2.getID());
	if(repr2.getID() != 6){
		printFailure("ID", "6");
	}
	printf("Repr width is %u\n", repr2.getWidth());
	if(repr2.getWidth() != 0){
		printFailure("width", "0");
	}
	printf("Repr height is %u\n", repr2.getHeight());
	if(repr2.getHeight() != 0){
		printFailure("height", "0");
	}
	printf("Repr bandwidth is %u\n", repr2.getBandwidth());
	if(repr2.getBandwidth() != 127236){
		printFailure("bandwidth", "127236");
	}
	printf("Repr BaseURL is %s\n", repr2.getBaseURL());
	if(repr2.getBaseURL() == NULL || strcmp(repr2.getBaseURL(), "car-20120827-8c.mp4") != 0){
		printFailure("BaseURL", "car-20120827-8c.mp4");
	}

	printf("Repr codecs is %s\n", repr2.getCodecs());
	if(strcmp(repr2.getCodecs(), "mp4a.40.2") != 0){
		printFailure("Codecs", "mp4a.40.2");
	}

	printf("Repr mimeType is %s\n", repr2.getMimeType());
	if(strcmp(repr2.getMimeType(), "audio/mp4") != 0){
		printFailure("mimeType", "audio/mp4");
	}
	printf("Repr audioSamplingRate is %u\n", repr2.getAudioSamplingRate());
	if(repr2.getAudioSamplingRate() != 44100){
		printFailure("audioSamplingRate", "44100");
	}
	printf("Repr startWithSAP is %d\n", repr2.getStartWithSAP());
	if(repr2.getStartWithSAP() != 0){
		printFailure("startWithSAP", "0");
	}
	printf("Size of segments is %u\n", (unsigned int)repr2.getSegs().size());
	if(repr2.getSegs().size() != 1){
		printFailure("Size of segments", "1");
	}
		
}
//MAKE SURE TO SORT WIDTH * height
		//SEPERATE AUDIO AND VIDEO PERIODS