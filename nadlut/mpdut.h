#include <stdio.h>
#include "mpd_parser.h"
#include "mpd.h"
#include <assert.h>

using namespace Nadl;
static char* carPath = "/data/local/car.mpd";
void printPeriod(Period& period, unsigned int& index);
void printAset(AdaptSet& aset, unsigned int& index);
void printRepr(Repr& repr, unsigned int& index);
void printSegment(Segment& segment, unsigned int& index);
void printFailure(char* variable, char const* actualValue);


void printAll(char* filePath);
void testCar(char* filePath);
void printUsage();