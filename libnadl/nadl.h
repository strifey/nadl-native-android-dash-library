#include <cstdlib>
#include <cstdint>
#include <cstdio>
#include <pthread.h>
#include "string.h"
#include "tinyxml.h"
#include "mpd.h"
#include "mpd_parser.h"
#include "nw_util.h"

#ifndef NADL_
#define NADL_


namespace Nadl{
	enum Quality{ WORST, BEST, AUTO, MANUAL};

	class NADL{
		private:
			uint16_t curr_pos;
			bool init;
			MPD mpd;
			Quality q_level;
			bool nw_setup();
		public:
			NADL(char* mpdfn);
			void play();
			void initialize();
			bool initCheck();
			//Set lowest/highest video/audio qualities
			//Flag to let us pick quality
	};
}

#endif
