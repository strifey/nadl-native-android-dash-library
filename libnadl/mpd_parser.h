#include <cstdlib>
#include <cstdio>
#include <utility>
#include <string.h>
#include <utils/Vector.h>
#include "mpd.h"
#include "adaptationset.h"
#include "representation.h"
#include "tinyxml.h"
#include "segment.h"
namespace Nadl{

	MPD parse_mpd(char*filename);
	Period createPeriod(TiXmlElement* parent);
	void initPeriodAttr(Period& period, TiXmlAttribute* attr);
	AdaptSet createAdaptSet(TiXmlElement* parent);
	void initAdaptSetAttr(AdaptSet& adaptset, TiXmlAttribute* attr);

	Repr createRepr(TiXmlElement* parent);
	void initReprAttr(Repr& rep, TiXmlAttribute* attr);
	char const* createBaseURL(TiXmlElement* parent);

	Segment createSegment(TiXmlElement* parent);
	void initSegmentAttr(Segment& segment, TiXmlAttribute* attr);

	std::pair<unsigned int, unsigned int> getInitRange(TiXmlElement* parent);


	bool parseBool(char const* boolean);
	bool parseBool(int boolean);
	std::pair<unsigned int, unsigned int> parseRange(char const* range);

}
