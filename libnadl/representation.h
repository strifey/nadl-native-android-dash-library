#include <cstdint>
#include <utils/Vector.h>
#include "segment.h"

#ifndef REPR_
#define REPR_

namespace Nadl{

class Repr {
	private:
		android::Vector<Segment> segs;
		char const* baseURL, *codecs, *mimeType;
		unsigned int id, width, height, bandwidth;
		uint16_t audioSamplingRate;
		unsigned int curr_segment;
		bool startWithSAP;
		//MimeType???
	public:
		//Replacement for constructor
		//Because it breaks Vector for some reason...
		void initRepr();

		unsigned int getID(){
			return id;
		}
		unsigned int getWidth(){
			return width;
		}
		unsigned int getHeight(){
			return height;
		}
		unsigned int getBandwidth(){
			return bandwidth;
		}
		unsigned int getAudioSamplingRate(){
			return audioSamplingRate;
		}
		bool getStartWithSAP(){
			return startWithSAP;
		}
		char const* getBaseURL(){
			return baseURL;
		}
		char const* getCodecs(){
			return codecs;
		}
		char const* getMimeType(){
			return mimeType;
		}

		android::Vector<Segment>& getSegs(){
			return segs;
		}

		void setID(unsigned int _id){
			id = _id;
		}

		void setWidth(unsigned int _width){
			width = _width;
		}

		void setHeight(unsigned int _height){
			height = _height;
		}
		void setBandwidth(unsigned int _bandwidth){
			bandwidth = _bandwidth;
		}

		void setAudioSamplingRate(unsigned int _audioSamplingRate){
			audioSamplingRate = _audioSamplingRate;
		}
		void setStartWithSAP(unsigned int _startWithSAP){
			startWithSAP =_startWithSAP;
		}

		void setBaseURL(char const* _baseURL){
			baseURL = _baseURL;
		}
		void setCodecs(char const* _codecs){
			codecs = _codecs;
		}
		void setMimeType(char const* _mimeType){
			mimeType = _mimeType;
		}


};

}
#endif
