#include "period.h"

namespace Nadl{

	Period::Period()
		:asets(), start(NULL), duration(NULL), curr_audio(0), curr_video(0){
	}

	android::Vector<AdaptSet>& Period::getAsets(){
		return asets;
	}

	char const* Period::getStart(){
		return start;
	}
	char const* Period::getDuration(){
		return duration;
	}

	void Period::setStart(char const* _start){
		start = _start;
	}
	void Period::setDuration(char const* _duration){
		duration = _duration;

	}

}
