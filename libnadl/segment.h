#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <utility>

#ifndef SEGMENT_
#define SEGMENT_

namespace Nadl{

	class Segment{
		private:
			std::pair<unsigned int, unsigned int> indexRange;
			std::pair<unsigned int, unsigned int> initRange;
			uint16_t startTime;
			bool indexRangeExact;
		public:
			void init();
			std::pair<unsigned int, unsigned int>& getIndexRange(){
				return indexRange;
			}
			std::pair<unsigned int, unsigned int>& getInitRange(){
				return initRange;
			}
			unsigned int getStartTime(){
				return startTime;
			}
			bool getIndexRangeExact(){
				return indexRangeExact;
			}

			void setIndexRange(unsigned int begin, unsigned int end){
				std::pair<unsigned int, unsigned int> newRange(begin, end);
				indexRange = newRange;
			}

			void setInitRange(unsigned int begin, unsigned int end){
				std::pair<unsigned int, unsigned int> newRange(begin, end);
				initRange = newRange;
			}
			void setIndexRange(std::pair<unsigned int, unsigned int> newRange){
				indexRange = newRange;
			}
			void setInitRange(std::pair<unsigned int, unsigned int> newRange){
				initRange = newRange;
			}

			void setStartTime(unsigned int _startTime){
				startTime = _startTime;
			}
			void setIndexRangeExact(bool _indexRangeExact){
				indexRangeExact = _indexRangeExact;
			}
	};
}
#endif
