#include <cstdint>
#include <utils/Vector.h>
#include "adaptationset.h"
#include "representation.h"
#include "segment.h"

#ifndef PERIOD_
#define PERIOD_

namespace Nadl{

class Period{
	private:
		android::Vector<AdaptSet> asets;
		char const* start;
		char const* duration;
		uint16_t curr_audio;
		uint16_t curr_video;
		//Type, profiles, minBuffertime, mediaPresentationDuration
	public:
		Period();
		android::Vector<AdaptSet>& getAsets();
		char const* getStart();
		char const* getDuration();
		void setStart(char const* _start);
		void setDuration(char const* _duration);
};

}

#endif
