#define UTIL_CPP
#include "nw_util.h"

namespace Nadl{
	static char* save_dir = "/data/local/";

	int nw_initvars(){
		pthread_mutex_init(&a_lock, NULL);
		pthread_mutex_init(&v_lock, NULL);
		pthread_cond_init(&aq_not_empty, NULL);
		pthread_cond_init(&vq_not_empty, NULL);
		pthread_cond_init(&a_fin_not_empty, NULL);
		pthread_cond_init(&v_fin_not_empty, NULL);
		stop = false;
		return 0;
	}

	char* requestMPD(nw_info *conn){
		int sock = connectToServer(conn);
		if(sock < 0)
			return NULL;
		//Send get request
		char* getmsg = createGetRequest(conn->url, conn->path);
		size_t total_sent = sendString(sock, getmsg);
		if(strlen(getmsg) > total_sent){
			free(getmsg);
			return NULL;
		}
		//TODO Actually receive data and return it
		char* save_path = (char*)
			malloc(sizeof(char)*(strlen(conn->path)+strlen(save_dir)));
		memcpy(save_path, save_dir, strlen(save_dir));
		save_path[strlen(save_dir)] = '\0';
		if(conn->path[0] == '/')
			strcat(save_path, &conn->path[1]);
		else
			strcat(save_path, conn->path);
		if(DEBUG)
			printf("Save path: %s\n", save_path);
		char* new_mpd = recvFile(sock, save_path);
		free(getmsg);
		close(sock);
		return new_mpd;
	}

	char* recvFile(int sock, char*filename){
		char buffer[BUFFSIZE];
		int total_recvd = 0;
		static char *ret = NULL;
		for(;;){
			int temp_recvd = recv(sock,&buffer, BUFFSIZE, MSG_DONTWAIT); 
			printf("Recieved %d bytes\n", temp_recvd);
			if(temp_recvd == -1 && errno == EAGAIN){
				if(DEBUG)
					printf("Returned EAGAIN\n");
				bool stuff = false;
				char count = 0;
				while(!stuff && count < 5){
					struct timeval tv;
					tv.tv_sec = 0;
					tv.tv_usec = 50000;
					fd_set readfds;
					FD_ZERO(&readfds);
					FD_SET(sock, &readfds);
					int sel_ret = select(sock+1, &readfds, NULL, NULL, &tv);
					if(sel_ret <= 0)
						count++;
					else stuff = true;
				}
				if(count == 5 && !stuff){
					if(DEBUG)
						printf("Waited and never got more data\n");
					break;
				}
				continue;
			}else if(temp_recvd < 1){
				if(DEBUG)
					printf("Definitely done receiving\n");
				break;
			}
			ret = (char*)realloc((void*)ret, total_recvd+temp_recvd);
			memcpy(&ret[total_recvd], &buffer, temp_recvd);
			total_recvd += temp_recvd;
		}
		if(DEBUG)
			printf("Received %d bytes\n", total_recvd);
		char *ret_data = strstr(ret, "\r\n\r\n");
		if(DEBUG)
			printf("Distance to actual data: %d\n", ret_data-ret);
		FILE*fd = fopen(filename, "w");
		if(fd)
			fwrite(ret_data+4, sizeof(char), total_recvd-((ret_data+4)-ret), fd);
		//the +4 is to get past the \r\n\r\n
		fclose(fd);
		free(ret);
		return filename;
		//return ret;
	}

	size_t sendString(int sock, char*msg){
		size_t total_sent = send(sock, (void*)msg, strlen(msg), 0);
		while(total_sent < strlen(msg)){
			size_t ssent = send(sock, (void*)&msg[total_sent], 
					strlen(&msg[total_sent]), 0);
			if(ssent < 1)
				break;
			total_sent += ssent;
		}
		if(DEBUG)
			printf("Sent string of %u size\n", total_sent);
		return total_sent;
	}

	char* createGetRequest(char*url, char*path){
		char *getmsg = (char*)malloc(sizeof(char)*100);
		strncat(getmsg, "GET ", 4);
		strncat(getmsg, path, strlen(path));
		strncat(getmsg, " HTTP/1.1\nHost: ", 16);
		strncat(getmsg, url, strlen(url));
		strncat(getmsg, "\n\n", 2);
		if(DEBUG)
			printf("Get request:\n\n%s\n", getmsg);
		return getmsg;
	}

	int aThead(){
		//connect
		//while buff not full
		//request & recv file
		//update buff_vector
		return 0;
	}

	int vThread(){
		//connect
		//while buff not full
		//request & recv file
		//update buff_vector
		struct nw_info *curr_file;
		int sock = connectToServer(&v_nw_info);
		if(sock < 1){
			perror("Couldn't connect to server with video files\n");
			return -1;
		}
		while(1){
			pthread_mutex_lock(&v_lock);
			while(v_file_q.empty()){
				pthread_cond_wait(&vq_not_empty, &v_lock);
			}
			curr_file = v_file_q.itemAt(0);
			v_file_q.removeAt(0);
			pthread_mutex_unlock(&v_lock);

			char* gr =  createGetRequest(v_nw_info.url, curr_file->path);
			size_t ssize = sendString(sock, gr);
			if(ssize < strlen(gr)){
				perror("We done goofed requesting a file!\n");
				exit(-1);
			}
			char* save_path = (char*)
				malloc(sizeof(char)*(strlen(curr_file->path)+strlen(save_dir)));
			memcpy(save_path, save_dir, strlen(save_dir));
			save_path[strlen(save_dir)] = '\0';
			if(curr_file->path[0] == '/')
				strcat(save_path, &curr_file->path[1]);
			else
				strcat(save_path, curr_file->path);
			if(DEBUG)
				printf("Save path: %s\n", save_path);
			char* finished_file = recvFile(sock, save_path);

			pthread_mutex_lock(&v_lock);
			v_finished_files.push_back(finished_file);
			pthread_cond_signal(&v_fin_not_empty);
			pthread_mutex_unlock(&v_lock);

			if(stop) break; // kinda dumb but need to be able to kill it
		}
		return 0;
	}

	int connectToServer(nw_info *conn){
		int sock = 0;
		struct addrinfo hints, *srvInfo, *pInfo;
		memset(&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		//hints.ai_flags = AI_PASSIVE;
		getaddrinfo(conn->url, conn->port, &hints, &srvInfo);
		if(srvInfo == NULL && DEBUG)
			perror("srvInfo is NULL!\n");
		for(pInfo = srvInfo; pInfo != NULL; pInfo = pInfo->ai_next){
			sock = socket(pInfo->ai_family, 
					pInfo->ai_socktype, pInfo->ai_protocol);
			if(sock <0){
				perror("Failed to create socket\n");
				continue;
			}
			if(connect(sock, pInfo->ai_addr, pInfo->ai_addrlen) < 0){
				close(sock);
				perror("Failed to connect to server\n");
				return -1;
			}
			if(DEBUG)
				printf("Found a socket and bound\n");
			break;
		}
		if(pInfo == NULL)
			perror("Failed to conect\n");
		if(srvInfo)
			freeaddrinfo(srvInfo);
		return sock;
	}
}
