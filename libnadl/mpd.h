#include <cstdlib>
#include <cstdio>
#include <utils/Vector.h>
#include <string.h>
#include "period.h"
#include "representation.h"
#include "segment.h"

#ifndef MPD_
#define MPD_

namespace Nadl{

class MPD{
	public:
		MPD();
		android::Vector<Period>& getPeriods();
	private:
		uint16_t curr_period;
		android::Vector<Period> periods;
		//Type, profiles, minBuffertime, mediaPresentationDuration
};

}

#endif
