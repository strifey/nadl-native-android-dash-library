#include "mpd_parser.h"

namespace Nadl{

	static int debug = 0;

	MPD parse_mpd(char* filename){

		//read in the document
		TiXmlDocument doc(filename);

		MPD mpd;
		//Check that it's valid
		if(!doc.LoadFile())
			return mpd;
		if(debug){
			printf("Doc is valid!\n");
		}
		//Create the mpd object
		//Probably need to change the arguments
		

		if(debug)
			printf("Getting periods\n");
		android::Vector<Period>& periods = mpd.getPeriods();
		if(debug)
			printf("Succeeded in getting periods\n");
		TiXmlNode* child = NULL;

		//Make sure to get the child from the root of the xml
		//Fix this later to add the things into the mpd
		TiXmlNode* parent = doc.FirstChild();

		//Iterate through all the children at the root of the xml docment
		while( (child = parent -> IterateChildren(child) ) ) {

			//Create the period and then add it to the mpd's vector
			//Make sure to cast as an TiXmlElement*
			Period period = createPeriod( child -> ToElement() );
			periods.push_back(period);
			if(debug){
				printf("Added a period to the mpd's list!\n");
			}
		}
		return mpd;
	}

	Period createPeriod(TiXmlElement* parent){
		Period period;
		//Get the the first attribute
		initPeriodAttr(period, parent->FirstAttribute());
		android::Vector<AdaptSet>& adaptset = period.getAsets();
		TiXmlNode* child = NULL;
		while( (child = parent -> IterateChildren(child) ) ) {
			AdaptSet aset = createAdaptSet(child -> ToElement());
			adaptset.push_back(aset);
		}
		return period;
	}

	void initPeriodAttr(Period& period, TiXmlAttribute* attr){
		while(attr != NULL){
			//Hardcode this for now
			//Make sure to change this later
			if(strcmp(attr -> Name(), "start") == 0){
				period.setStart(attr -> Value());
				if(debug){
					printf("Period's start is %s!\n", attr -> Value());
				}
			}
			else if(strcmp(attr -> Name(), "duration") == 0){
				period.setDuration(attr -> Value());
				if(debug){
					printf("Period's duration is %s!\n", attr -> Value());
				}
			}
			//Attributes are stored in a linkedlist manner
			attr=attr->Next();
		}
	}


	AdaptSet createAdaptSet(TiXmlElement* parent){
		AdaptSet aset = AdaptSet();

		//For some reason these need to be explicitly set?
		//I get random junk/segfaults if don't set these,
		//even though I give them default values in the constructor...
		//Need to look into this later
		aset.setSubsegment(false);
		aset.setMimeType(NULL);

		if(parent -> FirstAttribute() != NULL)
			initAdaptSetAttr(aset, parent->FirstAttribute());
		android::Vector<Repr>& reps = aset.getReps();
		TiXmlNode* child = NULL;
		while( (child = parent -> IterateChildren(child) ) ) {
			if(strcmp(child -> Value(), "Representation") == 0){
				Repr repr = createRepr(child -> ToElement());
				reps.push_back(repr);
			}
			else{
				if(debug)
					printf("Child value was %s\n", child -> Value());
			}
		}
		return aset;
	}
	void initAdaptSetAttr(AdaptSet& adaptset, TiXmlAttribute* attr){
		int count = 0;
		while(attr != NULL){
			//Hardcode this for now
			//Make sure to change this later
			printf("Attr number %d\n", count);
			if(strcmp(attr -> Name(), "subsegmentAlignment") == 0){
				adaptset.setSubsegment(parseBool(attr -> Value()));
				if(debug)
					printf("Adapt's subsegmentAlignment is %s! and the name is %s\n",
							attr -> Value(), attr -> Name());
			}
			else if(strcmp(attr -> Name(), "mimeType") == 0){
				adaptset.setMimeType(attr -> Value());
				if(debug)
					printf("Adapt's mimeType is %s! and the name is %s\n",
							attr -> Value(), attr -> Name());
			}
			//Attributes are stored in a linkedlist manner
			attr=attr->Next();
		}
	}


	Repr createRepr(TiXmlElement* parent){
		Repr rep;
		//Using this as the constructor as the original one breaks...
		rep.initRepr();
		initReprAttr(rep, parent -> FirstAttribute());
		android::Vector<Segment>& segments = rep.getSegs();
		TiXmlNode* child = NULL;
		while( (child = parent -> IterateChildren(child) ) ) {
			if(strcmp(child -> Value(), "SegmentBase") == 0){
				Segment segment = createSegment(child -> ToElement());
				segments.push_back(segment);
			}
			else if(strcmp(child -> Value(), "BaseURL") == 0){
				rep.setBaseURL(createBaseURL(child -> ToElement()));
			}	
			else{
				if(debug)
					printf("Child value was %s\n", child -> Value());
			}
		}
		return rep;
	}

	char const* createBaseURL(TiXmlElement* parent){
		return parent -> GetText();

	}

	void initReprAttr(Repr& rep, TiXmlAttribute* attr){
		while(attr != NULL){
			if(strcmp(attr -> Name(), "id") == 0){
				rep.setID(atoi(attr -> Value()));
				if(debug){
					printf("Repr's id is %u!\n", rep.getID());
				}
			}
			else if(strcmp(attr -> Name(), "width") == 0){
				rep.setWidth(atoi(attr -> Value()));
				if(debug){
					printf("Repr's width is %u!\n", rep.getWidth());
				}
			}
			else if(strcmp(attr -> Name(), "height") == 0){
				rep.setHeight(atoi(attr -> Value()));
				if(debug){
					printf("Repr's height is %u!\n", rep.getHeight());
				}
			}
			else if(strcmp(attr -> Name(), "bandwidth") == 0){
				rep.setBandwidth(atoi(attr -> Value()));
				if(debug){
					printf("Repr's bandwidth is %u!\n", rep.getBandwidth());
				}
			}
			else if(strcmp(attr -> Name(), "audioSamplingRate") == 0 ||
				strcmp(attr -> Name(), "sampleRate") == 0){
				rep.setAudioSamplingRate(atoi(attr -> Value()));
				if(debug){
					printf("Repr's bandwidth is %u!\n", rep.getAudioSamplingRate());
				}
			}
			else if(strcmp(attr -> Name(), "startWithSAP") == 0){
				rep.setStartWithSAP(parseBool(atoi(attr -> Value())));
				if(debug){
					printf("Repr's bandwidth is %u!\n", rep.getStartWithSAP());
				}
			}
			else if(strcmp(attr -> Name(), "BaseURL") == 0){
				rep.setBaseURL(attr -> Value());
				if(debug){
					printf("Repr's BaseURL is %s!\n", rep.getBaseURL());
				}
			}
			else if(strcmp(attr -> Name(), "codecs") == 0){
				rep.setCodecs(attr -> Value());
				if(debug){
					printf("Repr's codecs is %s!\n", rep.getCodecs());
				}
			}
			else if(strcmp(attr -> Name(), "mimeType") == 0){
				rep.setMimeType(attr -> Value());
				if(debug){
					printf("Repr's mimetype is %s!\n", rep.getMimeType());
				}
			}
			attr=attr->Next();
		}
	}

	Segment createSegment(TiXmlElement* parent){
		Segment segment;
		segment.init();
		initSegmentAttr(segment, parent -> FirstAttribute());
		TiXmlNode* child = NULL;
		while( (child = parent -> IterateChildren(child) ) ) {
			if(strcmp(child -> Value(), "Initialization") == 0){
				std::pair<unsigned int, unsigned int> initRange = getInitRange(child -> ToElement());
				segment.setInitRange(initRange);
			}
			else{
				if(debug)
					printf("Initialization not found, Child value was %s\n", child -> Value());
			}
		}
		return segment;
	}

	void initSegmentAttr(Segment& segment, TiXmlAttribute* attr){
		while(attr != NULL){
			if(strcmp(attr -> Name(), "indexRange") == 0){
				segment.setIndexRange(parseRange(attr -> Value()));
				if(debug){
					printf("Segments's indexRange is [%u, %u]!\n", segment.getIndexRange().first, segment.getIndexRange().second);
				}
			}
			else if(strcmp(attr -> Name(), "indexRangeExact") == 0){
				segment.setIndexRangeExact(parseBool(attr -> Value()));
				if(debug){
					printf("Segments's indexRangeExact is %d\n", segment.getIndexRangeExact());
				}
			}
			attr=attr->Next();
		}
	}

	std::pair<unsigned int, unsigned int> getInitRange(TiXmlElement* parent){

		TiXmlAttribute* attr = parent -> FirstAttribute();
		if(strcmp(attr -> Name(), "range") == 0){
			if(debug)
				printf("InitRange was found!\n");
			return parseRange(attr -> Value());
		}
		printf("InitRange was not found!\n");
		return std::pair<unsigned int, unsigned int>(0, 0);
	}

	bool parseBool(char const* boolean){
		if(strcmp(boolean, "true") == 0){
			return true;
		}
		return false;
	}
	bool parseBool(int boolean){
		if(boolean == 0)
			return false;
		return true;
	}
	std::pair<unsigned int, unsigned int> parseRange(char const* range){
		int begin = 0;
		int end = 0;
		char* newRange = (char*)malloc(sizeof(range) * strlen(range));
		newRange = strcpy(newRange, range);
		char* temp = strtok(newRange, "-");
		begin = atoi(temp);
		temp = strtok(NULL, "-");
		end = atoi(temp);
		free(newRange);
		return std::pair<unsigned int, unsigned int>(begin, end);
	}
}

//vector stuff is at
//AOSP/frameworks/native/include/utils
//

