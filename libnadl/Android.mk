LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

APP_STL := stlport_static
LOCAL_SRC_FILES := nadl.cpp mpd.cpp period.cpp representation.cpp segment.cpp \
   mpd_parser.cpp nw_util.cpp
LOCAL_SHARED_LIBRARIES    := libstdc++ liblog libutils libbinder libcutils \
	libdl libtinyxml

LOCAL_SHARED_LIBRARIES += libstlport
include external/stlport/libstlport.mk

LOCAL_C_INCLUDES := system/extras/libtinyxml
LOCAL_MODULE := libnadl
LOCAL_MODULE_TAGS := optional
LOCAL_LDLIBS    := -llog
include $(BUILD_SHARED_LIBRARY)
