LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := tinyxml.cpp tinyxmlparser.cpp xmltest.cpp\
	tinyxmlerror.cpp tinystr.cpp
LOCAL_SHARED_LIBRARIES    := libstdc++ liblog libutils libbinder libcutils \
	libdl
LOCAL_MODULE := libtinyxml
LOCAL_MODULE_TAGS := optional
LOCAL_C_INCLUDES :=

include $(BUILD_SHARED_LIBRARY)
