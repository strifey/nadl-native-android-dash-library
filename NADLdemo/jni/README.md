#NADL 4803 Project

##Attempt to port functionality of MPEG-DASH streaming protocol to be used in Android

##How to install:
* Install AOSP
* Set up build environment for AOSP as described on the AOSP website

* Place libtinyxml and libnadl into <aosp>/system/extras 
* Run mm in libtinyxml and libnadl in that order
* Move to <aosp> root directory and 'make snod'
