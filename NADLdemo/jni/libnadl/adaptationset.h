#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <vector>
#include "representation.h"
#include "segment.h"

#ifndef ADAPT_SET_
#define ADAPT_SET_

namespace Nadl{

class AdaptSet{
	private:
		std::vector<Repr> reps;
		bool subsegmentAlign;
		char const* mimeType;
		uint16_t curr_repr;
	public:
		AdaptSet(){}
		void setSubsegment(bool _subsegmentAlign){
			subsegmentAlign = _subsegmentAlign;
		}
		void setMimeType(char const* _mimeType){
			mimeType = _mimeType;
		}

		bool getSubsegment(){
			return subsegmentAlign;
		}
		char const* getMimeType(){
			return mimeType;
		}
		std::vector<Repr>& getReps(){
			return reps;
		}
};
}

#endif
