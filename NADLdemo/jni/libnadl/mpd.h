#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <vector>
#include "period.h"
#include "representation.h"
#include "segment.h"

#ifndef MPD_
#define MPD_

namespace Nadl{

class MPD{
	public:
		MPD();
		std::vector<Period>& getPeriods();
	private:
		uint16_t curr_period;
		std::vector<Period> periods;
		//Type, profiles, minBuffertime, mediaPresentationDuration
};

}

#endif
