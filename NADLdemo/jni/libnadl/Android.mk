LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

APP_STL := stlport_shared
LOCAL_SRC_FILES := nadl.cpp mpd.cpp period.cpp representation.cpp segment.cpp \
   mpd_parser.cpp nw_util.cpp
LOCAL_SHARED_LIBRARIES    := libstdc++ liblog libutils libbinder  \
	libdl libtinyxml

LOCAL_C_INCLUDES := ../libtinyxml
LOCAL_MODULE := libnadl
LOCAL_MODULE_TAGS := optional
LOCAL_LDLIBS    := -llog
include $(BUILD_SHARED_LIBRARY)
