#include <utility>
#include <cstdlib>
#include <cstdint>
#include <cstdio>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <vector>

#define GET_REQ(file, URL) ("GET " file " HTTP/1.1\nHost: " url"\n\n")
#define DEBUG (1)

#ifdef UTIL_CPP
	#define EXTERN
#else
	#define EXTERN extern
#endif

#ifndef NW_UTIL_H_
#define NW_UTIL_H_

namespace Nadl{

#define BUFFSIZE (1500)

	struct nw_info{
		char* port;
		char* url;
		char* path;
		std::pair<int, int> file_range;
	};

	EXTERN std::vector<char*> a_finished_files;
	EXTERN std::vector<char*> v_finished_files;
	EXTERN std::vector<nw_info*> a_file_q;
	EXTERN std::vector<nw_info*> v_file_q;
	EXTERN pthread_mutex_t a_lock, v_lock;
	EXTERN pthread_cond_t aq_not_empty, vq_not_empty;
	EXTERN pthread_cond_t a_fin_not_empty, v_fin_not_empty;
	EXTERN nw_info v_nw_info;
	EXTERN nw_info a_nw_info;
	EXTERN bool stop;

	int nw_initvars();
	int nw_cleanup();
	char* requestMPD(nw_info *conn);
	int aThread();
	int vThread();
	int connectToServer(nw_info *conn);
	char* createGetRequest(char*url, char*path);
	size_t sendString(int sock, char*msg);
	char* recvFile(int sock, char*filename);
}

#endif
